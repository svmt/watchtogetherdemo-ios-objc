//
//  ViewController.m
//  ObjectiveC Lib Example
//
//  Created by Sceenic on 6/19/19.
//  Copyright © 2019 Sceenic. All rights reserved.
//

#import "SessionViewController.h"
#import <WatchTogetherLib/Session.h>
#import "WebRTCCollectionViewCell.h"
#import "SettingsViewController.h"
#import <AVKit/AVKit.h>
#import <CoreMedia/CoreMedia.h>

@interface SessionViewController () <UICollectionViewDelegate, UICollectionViewDataSource, SessionDelegate, ReconnectListener, WebRTCUserCollectionViewCellDelegate, SettingsViewControllerDelegate> {
    BOOL isFrontCamera;
    BOOL isOrganiser;
    BOOL needToReconnect;
    NSTimer *sessionTimer;
    int seconds;
    WTParticipantType currentParticipantType;
    VideoResolution currentVideoResolution;
    int currentFrameRate;
}

@property (weak, nonatomic) IBOutlet UICollectionView *userStreamsCollection;

@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UILabel *timerLable;

@property (nonatomic, strong) Session *sessionManager;
@property(strong, nonatomic) NSMutableArray *participantsArray;

@end

static NSString* const kAccessToken = @"";

@implementation SessionViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSAssert(kAccessToken.length > 0, @"Streaming token is empty");
    
    seconds = 0;
    currentParticipantType = WTFullParticipant;
    currentVideoResolution = VideoResolutionDefault;
    currentFrameRate = 30;
    self.participantsArray = [NSMutableArray new];
    isFrontCamera = YES;
    
    [Session setWebRTCLogLevel:WTWebRTCLogLevelError];
    self.sessionManager = [Session build:^(id<SessionBuilder> builder) {
        builder.accessToken = kAccessToken;
        builder.username = self.userName;
        builder.delegate = self;
        builder.reconnectListener = self;
    }];
    [self.sessionManager setupSession];
}

-(void) startTimer{
    sessionTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f  target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
}

- (void)updateTimer:(NSTimer *)theTimer {
    //only for demonstrate work of sync
        seconds++;
        int min = seconds/60;
        int sec = seconds%60;
    [_timerLable setText:[NSString stringWithFormat:@"%01d:%02d",min,sec]];
    //------------------------------------------//
    if(seconds % 10 == 0){
        //        [avPlayer setRate:1];
    }
    if(seconds % 3 == 0){
        //    CMTime timePlayer = [[avPlayer currentItem] currentTime];
        //    [_sessionManager sendPlayerData: [[NSNumber numberWithInt: CMTimeGetSeconds(timePlayer)] stringValue]];
        [_sessionManager sendPlayerData: [[NSNumber numberWithInt: seconds] stringValue]];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showSettings"]) {
        SettingsViewController *settingsVC = segue.destinationViewController;
        settingsVC.delegate = self;
        settingsVC.currentVideoResolution = currentVideoResolution;
        settingsVC.currentFrameRate = currentFrameRate;
        settingsVC.currentParticipantType = currentParticipantType;
    }
}

#pragma mark - Actions

- (IBAction)onCamera:(id)sender {
#if TARGET_OS_EMBEDDED
    [_sessionManager startCameraPreview];
    [self.cameraButton setHidden:YES];
#endif
}

- (IBAction)onConnect:(id)sender {
    [self connectToSession];
}

- (IBAction)onDisconnect:(id)sender {
    if (![_sessionManager isConnected]) {
        return;
    }
    
    [_sessionManager disconnect];
}

- (IBAction)onSettings:(id)sender {
    [self performSegueWithIdentifier:@"showSettings" sender:self];
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.participantsArray count];
}

-(UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"userCell";
    
    WebRTCCollectionViewCell* streamCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    streamCell.delegate = self;
    
    Participant *participant = self.participantsArray[indexPath.row];
    [streamCell displayParticipant:participant isFrontCamera:isFrontCamera];
    
    return streamCell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect collectionBounds = self.userStreamsCollection.bounds;
    CGFloat collectionWidth = collectionBounds.size.width;
    return CGSizeMake(collectionWidth * 0.4, collectionWidth * 0.5);
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {

        [self.userStreamsCollection performBatchUpdates:^{

            [self.userStreamsCollection setCollectionViewLayout:self.userStreamsCollection.collectionViewLayout animated:YES];
            [self.userStreamsCollection reloadData];
        } completion:nil];
    }];
}


#pragma mark - SessionDelegate

- (void)SessionConnected:(NSString *)sessionId participants:(NSArray<NSString *> *)existPublisher {
    NSLog(@"Session %@ connected, existing publishers %@", sessionId, [existPublisher componentsJoinedByString:@","]);
    self.cameraButton.hidden = YES;
    self.connectButton.hidden = YES;
    [self startTimer];
    [[UIApplication sharedApplication] setIdleTimerDisabled: YES];
}

- (void)SessionOnLocalParticipantJoined:(Participant *)participant{
    if (participant) {
        NSLog(@"Session local participant joined, id %@, display name %@", [participant getID], [participant getDisplayName]);
        
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.userStreamsCollection performBatchUpdates:^{
            [self.participantsArray insertObject:participant atIndex:0];
            [self.userStreamsCollection insertItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        } completion:nil];
    }
}

- (void)SessionParticipantJoined:(Participant *)participant {
    [self addParticipantToUI:participant];
}

- (void)SessionParticipantLeft:(Participant *)participant {
    [self removeParticipantFromUI:participant];
}

- (void)sessionDidSubscribeToParticipant:(Participant *)participant {
    NSLog(@"Session did subscribe to participant %@", [participant getID]);
    [self addParticipantToUI:participant];
}

- (void)sessionDidUnsubscribeFromParticipant:(Participant *)participant {
    NSLog(@"Session did unsubscribe from participant %@", [participant getID]);
    [self removeParticipantFromUI:participant];
}

- (void)SessionBecomesAnOrganizer:(BOOL)isAnOrganizer {
    isOrganiser = isAnOrganizer;
}

- (void)SessionOnManageParticipantMedia:(NSString *)participantID mediaType:(MediaType)mediaType mediaState:(MediaState)mediaState {
    NSLog(@"Session did change participant %@ media state, media type %li, media state %li", participantID, mediaType, mediaState);
    Participant *participant = [self participantWithId:participantID];
    if (participant) {
        switch (mediaType) {
            case AUDIO:
                switch (mediaState) {
                    case ENABLED:
                        [participant setIsAudioEnabled:YES];
                        break;
                    case DISABLED:
                        [participant setIsAudioEnabled: NO];
                        break;
                    default:
                        break;
                }
                break;
            
            case VIDEO:
                switch (mediaState) {
                    case ENABLED:
                        [participant setIsVideoEnabled:YES];
                        break;
                    case DISABLED:
                        [participant setIsVideoEnabled:NO];
                        break;
                    default:
                        break;
                }
                break;
            
            case ALL:
                switch (mediaState) {
                    case ENABLED:
                        participant.isVideoEnabled = YES;
                        participant.isAudioEnabled = YES;
                        break;
                    case DISABLED:
                        participant.isVideoEnabled = NO;
                        participant.isAudioEnabled = NO;
                        break;
                    default:
                        break;
                }
                break;
        }
        
        NSUInteger index = [_participantsArray indexOfObject:participant];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.userStreamsCollection reloadItemsAtIndexPaths:@[indexPath]];
    }
}

- (void)SessionDidGetStreamTime:(NSString *)time withUserNumber:(NSInteger)userNumber {
    
    //only for test
    seconds = [time intValue];
    //------------------------------------------//
    
//    double newTime = [time doubleValue];
//    CMTime timePlayer = [[avPlayer currentItem] currentTime];
//    double playerTime = CMTimeGetSeconds(timePlayer);
//    double delta = newTime - playerTime;
//    if(fabs(delta)<0.3){
//        [avPlayer setRate:1];
//        return;
//    }
//
//    if(delta > 11){
//        [avPlayer seekToTime:CMTimeMake(newTime, 1)];
//        return;
//    }
//    if (delta > 10){
//        [avPlayer setRate:1.7];
//        return;
//    } else if (delta > 0){
//        [avPlayer setRate:1.4];
//        return;
//    } else if (delta < -10){
//        [avPlayer setRate:0.6];
//        return;
//    } else if (delta < 0){
//        [avPlayer setRate:0.8];
//        return;
//    }
}

- (void)participant:(Participant *)participant streamQualityChanged:(WTStreamQuality)quality mosAudio:(float)mosAudio mosVideo:(float)mosVideo {
        int i = 0;
        for (Participant *participantFromArray in _participantsArray) {
            if ([[participant getID] isEqualToString:[participantFromArray getID]]) {
//                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
//                NSError * err;
//                NSData * jsonData = [NSJSONSerialization dataWithJSONObject:stats options:0 error:&err];
                NSString * myString = [NSString stringWithFormat:@"Quality %@\n mosAudio %f\n mosVideo %f", [self stringFromStreamQuality:quality], mosAudio, mosVideo];//[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                dispatch_async(dispatch_get_main_queue(), ^{
                    WebRTCCollectionViewCell* cell = [[WebRTCCollectionViewCell alloc] init];//[_userStreamsCollection cellForItemAtIndexPath:indexPath];
//                    [cell.statTextView insertText:@"\n<----->\n"];
//                    [cell.statTextView insertText:myString];
                    cell.statTextView.text = myString;
//                    [self scrollTextViewToBottom:cell.statTextView];
    //                [ scroll]
    //                cell.statTextView.text += myString;
                });
            }
            ++i;
        }
//    NSLog(@"Participant %@ stream quality changed, new quality %@, mos %f", [participant getConnectionName], [self stringFromStreamQuality:quality], mos);
}

- (void)SessionDidFailWithError:(NSError *)error {
    NSLog(@"Session did fail with error %@", error);
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:nil];
    [alert addAction:defaultAction];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
        [[UIApplication sharedApplication] setIdleTimerDisabled: NO];
    });
}

- (void)SessionDidFinish {
    if (needToReconnect) {
        self.sessionManager = [Session build:^(id<SessionBuilder> builder) {
            builder.accessToken = kAccessToken;
            builder.username = self.userName;
            builder.delegate = self;
        }];
        [self.sessionManager setupSession];
        [self connectToSession];
    } else {
        isOrganiser = NO;
        [sessionTimer invalidate];
        sessionTimer = nil;
        self.sessionManager = nil;
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [[UIApplication sharedApplication] setIdleTimerDisabled: NO];
}

#pragma mark - ReconnectListener

- (void)SessionOnLocalParticipantReconnecting {
    
}

- (void)SessionOnLocalParticipantReconnected {
    
}

- (void)SessionOnRemoteParticipantReconnecting:(Participant *)participant {
    
}

- (void)SessionOnRemoteParticipantReconnected:(Participant *)participant {
    
}

#pragma mark - WebRTCUserCollectionViewCellDelegate

- (void)changeVolume: (double)volume cell: (WebRTCCollectionViewCell*)cell{
    NSIndexPath* selectedIndexPath = [_userStreamsCollection indexPathForCell:cell];
       Participant* participant = self.participantsArray[selectedIndexPath.row];
    [participant setVolume:volume];
}

- (void)enableAudioOnCell:(WebRTCCollectionViewCell *)cell{
    NSIndexPath* selectedIndexPath = [_userStreamsCollection indexPathForCell:cell];
    Participant* participant = self.participantsArray[selectedIndexPath.row];
    
    if (!participant.isAudioEnabled){
        [participant enableAudioWithCompletion:nil];
    } else {
        [participant disableAudioWithCompletion:nil];
    }
}

- (void)enableVideoOnCell:(WebRTCCollectionViewCell*)cell {
    NSIndexPath* selectedIndexPath = [_userStreamsCollection indexPathForCell:cell];
    Participant* participant = self.participantsArray[selectedIndexPath.row];
    
    if (!participant.isVideoEnabled) {
        [participant enableVideoWithCompletion:nil];
    } else {
        [participant disableVideoWithCompletion:nil];
    }
}

- (void)switchCameraOnCell:(WebRTCCollectionViewCell *)cell{
#if TARGET_OS_EMBEDDED
    if(isFrontCamera){
        [_sessionManager switchCameraTo:AVCaptureDevicePositionBack];
    } else {
        [_sessionManager switchCameraTo:AVCaptureDevicePositionFront];
    }
    isFrontCamera = !isFrontCamera;
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_userStreamsCollection reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
#endif
}

#pragma mark - SettingsViewControllerDelegate

- (void)participantTypeDidChange:(WTParticipantType)newType {
    currentParticipantType = newType;
    [self.sessionManager transitionParticipantTypeTo:currentParticipantType];
}

- (void)videoResolutionDidChange:(VideoResolution)newResolution {
    currentVideoResolution = newResolution;
    CGSize newVideoSize = [SettingsViewController sizeFromVideoResolution:currentVideoResolution];
    [self.sessionManager adjustResolutionWidth:newVideoSize.width height:newVideoSize.height];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.userStreamsCollection reloadItemsAtIndexPaths:@[indexPath]];
}

- (void)frameRateDidChange:(int)newFps {
    currentFrameRate = newFps;
    [self.sessionManager adjustFrameRate:newFps];
}

#pragma mark - Utils

- (void)showNotAuthorizedAlert {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loginToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"Not authorized"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
        
        [self.delegate popViewController];
    }];
    
    [alert addAction:defaultAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (Participant*)participantWithId:(NSString*)participantId {
    for (Participant *participant in _participantsArray) {
        if ([[participant getID] isEqualToString:participantId]) {
            return participant;
        }
    }
    
    return nil;
}

- (BOOL)removeParticipant:(Participant*)participant {
    NSString *participantId = [participant getID];
    Participant *oldParticipant = [self participantWithId:participantId];
    if (oldParticipant) {
        [_participantsArray removeObject:oldParticipant];
        return YES;
    }
    return NO;
}

- (void)rtcStats:(NSDictionary *)stats fromParticipant:(Participant *)participant{

    NSLog(@"%@: %@", [participant getDisplayName], stats);
}

-(void)scrollTextViewToBottom:(UITextView *)textView {
     if(textView.text.length > 0 ) {
        NSRange bottom = NSMakeRange(textView.text.length -1, 1);
        [textView scrollRangeToVisible:bottom];
     }

}

- (NSString*)stringFromStreamQuality:(WTStreamQuality)quality {
    switch (quality) {
        case WTStreamQualityBad:
            return @"Bad quality";
            break;
            
        case WTStreamQualityGood:
            return @"Good quality";
            break;
            
        case WTStreamQualityExcellent:
            return @"Excellen quality";
            break;
    }
}

- (void) addParticipantToUI: (Participant *) participant{
    if (participant) {
        //        [participant startStatsWithInterval:5];
        NSUInteger participantInex = [_participantsArray indexOfObject:participant];
        
        
        [self.userStreamsCollection performBatchUpdates:^{
            if ([self removeParticipant:participant]){
                if (participantInex < _participantsArray.count){
                    [_participantsArray insertObject:participant atIndex:participantInex];
                    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:participantInex inSection:0];
                    [_userStreamsCollection reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
                    
                }
            } else {
                [self.participantsArray addObject:participant];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_participantsArray.count - 1 inSection:0];
                NSArray * indexPaths = @[indexPath];
                [self.userStreamsCollection insertItemsAtIndexPaths:indexPaths];
            }
        } completion:^(BOOL finished) {
        }];
    }
}

- (void) removeParticipantFromUI: (Participant *) participant{
        if (participant) {
        NSUInteger index = [self.participantsArray indexOfObject:participant];
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        if (index >= _participantsArray.count){
            return;
        }
            [self.userStreamsCollection performBatchUpdates:^{
                NSLog(@"Index for remove: %lu", (unsigned long)index);
                [self.participantsArray removeObject:participant];
                [self.userStreamsCollection deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            } completion:^(BOOL finished) {

            }];
    }
}

- (void) connectToSession {
#if TARGET_IPHONE_SIMULATOR
    [_sessionManager connectWithMode:WTViewer];
#elif TARGET_OS_IOS
    [_sessionManager connectWithMode:currentParticipantType];
#else
    [_sessionManager connect];
#endif
}

@end
