//
//  SettingsViewController.h
//  ObjectiveC Lib Example
//
//  Created by Igor Nazarov on 21.09.2020.
//  Copyright © 2020 Sceenic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WatchTogetherLib/WTDataTypes.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, VideoResolution) {
    VideoResolutionLow,
    VideoResolutionDefault,
    VideoResolutionHigh
};

@protocol SettingsViewControllerDelegate <NSObject>

@optional
- (void)participantTypeDidChange:(WTParticipantType)newType;
- (void)videoResolutionDidChange:(VideoResolution)newResolution;
- (void)frameRateDidChange:(int)newFps;

@end

@interface SettingsViewController : UIViewController

@property (nonatomic, weak) id<SettingsViewControllerDelegate> delegate;
@property (nonatomic) WTParticipantType currentParticipantType;
@property (nonatomic) VideoResolution currentVideoResolution;
@property (nonatomic) int currentFrameRate;

+ (CGSize)sizeFromVideoResolution:(VideoResolution)resolution;

@end

NS_ASSUME_NONNULL_END
