//
//  WebRTCCollectionViewCell.m
//  DemoWatchTogetherObjC
//
//  Created by Sceenic on 1/23/19.
//  Copyright © 2019 Sceenic. All rights reserved.
//

#import "WebRTCCollectionViewCell.h"
#import <WatchTogetherLib/Session.h>
#import <WatchTogetherLib/Participant.h>

@interface WebRTCCollectionViewCell() {
    Participant *currentParticipant;
}

@property (weak, nonatomic) IBOutlet UIButton *audioButton;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;
@property (weak, nonatomic) IBOutlet UIButton *switchCameraButton;
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;
@property (weak, nonatomic) IBOutlet UIButton *statButton;

@end

@implementation WebRTCCollectionViewCell

- (void)displayParticipant:(Participant*)participant isFrontCamera:(BOOL)isFrontCamera {
    BOOL isLocal = participant.isLocalParticipant;
    UIView *videoView = [participant getVideo];
    [self displayRenderView:videoView isLocalVideo:isLocal isFrontCamera:isFrontCamera];
    
    currentParticipant = participant;
    
    self.switchCameraButton.hidden = !isLocal;
    [self enableAudio:currentParticipant.isAudioEnabled];
    [self enableVideo:currentParticipant.isVideoEnabled];
}

- (void)displayRenderView:(UIView*)renderView isLocalVideo:(BOOL)isLocalVideo isFrontCamera:(BOOL)isFrontCamera {
    renderView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.contentView addSubview:renderView];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeTop multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeBottom multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeLeading multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeTrailing multiplier:1. constant:0.]];
    
    [self layoutIfNeeded];
    
    if (isLocalVideo && isFrontCamera) {
        renderView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    }
    
    [self.contentView bringSubviewToFront:self.audioButton];
    [self.contentView bringSubviewToFront:self.videoButton];
    [self.contentView bringSubviewToFront:self.switchCameraButton];
    [self.contentView bringSubviewToFront:self.volumeSlider];
    [self.contentView bringSubviewToFront:self.statTextView];
    [self.contentView bringSubviewToFront:self.statButton];
}

- (void)enableAudio:(BOOL)enabled {
    NSString *audioImageName = enabled ? @"ic_bottom_mic_on" : @"ic_bottom_mic_off";
    UIImage *audioImage = [UIImage imageNamed:audioImageName];
    [self.audioButton setImage:audioImage forState:UIControlStateNormal];
}

- (void)enableVideo:(BOOL)enabled {
    NSString *videoImageName = enabled ? @"ic_bottom_video_on" : @"ic_bottom_video_off";
    UIImage *videoImage = [UIImage imageNamed:videoImageName];
    [self.videoButton setImage:videoImage forState:UIControlStateNormal];
}

#pragma mark - Actions

- (IBAction)muteAudioAction:(id)sender {
    [self.audioButton setEnabled:NO];
    [self enableAudio:!currentParticipant.isAudioEnabled];
    
    [self.delegate enableAudioOnCell:self];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.audioButton setEnabled:YES];
    });
}

- (IBAction)muteVideoAction:(id)sender {
    [self.videoButton setEnabled:NO];
    [self enableVideo:!currentParticipant.isVideoEnabled];
    
    [self.delegate enableVideoOnCell:self];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.videoButton setEnabled:YES];
    });
}

- (IBAction)switchCameraAction:(id)sender {
    [self.delegate switchCameraOnCell:self];
}

- (IBAction)changeVolume:(UISlider*)sender {
    [self.delegate changeVolume:[sender value] cell:self];
}

- (IBAction)infoAction:(id)sender {
    self.statTextView.hidden = !_statTextView.hidden;
}

@end
