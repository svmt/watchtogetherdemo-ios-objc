//
//  ViewController.h
//  ObjectiveC Lib Example
//
//  Created by Sceenic on 6/19/19.
//  Copyright © 2019 Sceenic. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PopController <NSObject>

-(void)popViewController;

@end

@interface SessionViewController : UIViewController

@property (weak) id<PopController> delegate;
@property (nonatomic) NSString* userName;
@property (nonatomic) NSString* sessionName;

@end

