//
//  CredentialsViewController.m
//  ObjectiveC Lib Example
//
//  Created by Sceenic on 7/17/19.
//  Copyright © 2019 Sceenic. All rights reserved.
//

#import "CredentialsViewController.h"
#import "SessionViewController.h"

@interface CredentialsViewController ()<UITextFieldDelegate, PopController> {
}
@property (weak, nonatomic) IBOutlet UITextField *displayNameTextField;

@end
@implementation CredentialsViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.userName = [[NSUserDefaults standardUserDefaults] valueForKey:@"userName"];
}
- (IBAction)joinAction:(id)sender {
    [self performSegueWithIdentifier:@"loginToStream" sender:nil];
}

-(void) popViewController {
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];

    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[SessionViewController class]]) {
        SessionViewController* vc = segue.destinationViewController;
        vc.userName = self.displayNameTextField.text;
        vc.delegate = self;
    }
}

@end
