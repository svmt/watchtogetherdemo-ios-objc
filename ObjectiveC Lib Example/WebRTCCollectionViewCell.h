//
//  WebRTCCollectionViewCell.h
//  DemoWatchTogetherObjC
//
//  Created by Sceenic on 1/23/19.
//  Copyright © 2019 Sceenic. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WebRTCCollectionViewCell;
@class Participant;

@protocol WebRTCUserCollectionViewCellDelegate <NSObject>

- (void)enableAudioOnCell:(WebRTCCollectionViewCell*)cell;
- (void)enableVideoOnCell:(WebRTCCollectionViewCell*)cell;
- (void)switchCameraOnCell:(WebRTCCollectionViewCell*)cell;
- (void)changeVolume:(double)volume cell:(WebRTCCollectionViewCell*)cell;

@end

@interface WebRTCCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UITextView *statTextView;

@property (weak, nonatomic) id<WebRTCUserCollectionViewCellDelegate> delegate;

- (IBAction)muteAudioAction:(id)sender;
- (IBAction)muteVideoAction:(id)sender;

- (void)displayParticipant:(Participant*)participant isFrontCamera:(BOOL)isFrontCamera;

@end


