//
//  SettingsViewController.m
//  ObjectiveC Lib Example
//
//  Created by Igor Nazarov on 21.09.2020.
//  Copyright © 2020 Sceenic. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UISlider *fpsSlider;
@property (weak, nonatomic) IBOutlet UISegmentedControl *videoResolutionSegmentedControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *participantTypeSegmentedControl;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.participantTypeSegmentedControl.selectedSegmentIndex = self.currentParticipantType;
    self.videoResolutionSegmentedControl.selectedSegmentIndex = self.currentVideoResolution;
    self.fpsSlider.value = self.currentFrameRate;
}

#pragma mark - Actions

- (IBAction)onDismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)adjustFrameRate:(UISlider*)sender {
    int fps = (int)sender.value;
    
    if ([self.delegate respondsToSelector:@selector(frameRateDidChange:)]) {
        [self.delegate frameRateDidChange:fps];
    }
}

- (IBAction)adjustVideoResolution:(UISegmentedControl*)sender {
    VideoResolution resolution = [self selectedVideoResolution];
    
    if ([self.delegate respondsToSelector:@selector(videoResolutionDidChange:)]) {
        [self.delegate videoResolutionDidChange:resolution];
    }
}

- (IBAction)participantTypeChanged:(id)sender {
    WTParticipantType type = [self selectedParticipantType];
    
    if ([self.delegate respondsToSelector:@selector(participantTypeDidChange:)]) {
        [self.delegate participantTypeDidChange:type];
    }
}

#pragma mark - Utils

- (WTParticipantType)selectedParticipantType {
    WTParticipantType participantType;
    
    switch (self.participantTypeSegmentedControl.selectedSegmentIndex) {
        case 0:
            participantType = WTFullParticipant;
            break;
        case 1:
            participantType = WTBroadcaster;
            break;
        default:
            participantType = WTViewer;
            break;
    }
    
    return participantType;
}

- (VideoResolution)selectedVideoResolution {
    VideoResolution resolution;
    
    switch (self.videoResolutionSegmentedControl.selectedSegmentIndex) {
        case 0:
            resolution = VideoResolutionLow;
            break;
        case 1:
            resolution = VideoResolutionDefault;
            break;
        default:
            resolution = VideoResolutionHigh;
            break;
    }
    
    return resolution;
}

+ (CGSize)sizeFromVideoResolution:(VideoResolution)resolution {
    int width = 0;
    int height = 0;
    
    switch (resolution) {
        case VideoResolutionLow:
            width = 192;
            height = 144;
            break;
            
        case VideoResolutionDefault:
            width = 480;
            height = 360;
            break;
            
        case VideoResolutionHigh:
            width = 640;
            height = 480;
            break;
    }
    
    return CGSizeMake(width, height);
}

@end
