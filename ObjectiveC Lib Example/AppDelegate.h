//
//  AppDelegate.h
//  ObjectiveC Lib Example
//
//  Created by Sceenic on 6/19/19.
//  Copyright © 2019 Sceenic. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

