//
//  CredentialsViewController.h
//  ObjectiveC Lib Example
//
//  Created by Sceenic on 7/17/19.
//  Copyright © 2019 Sceenic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CredentialsViewController : UIViewController

@property (weak, nonatomic) NSString* userName;

@end


